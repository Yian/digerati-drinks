/**
 * Created by yiannis.pipertzis on 30/04/2015.
 */
UserPaymentsCollection = new Mongo.Collection('userpayments');

UserPaymentsSchema = new SimpleSchema({
  userId: {
    type: String
  },
  amountPaid: {
    type: Number
  },
  dateOfPayment: {
    type: Date
  }
});

UserPaymentsCollection.attachSchema(UserPaymentsSchema);