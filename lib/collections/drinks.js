/**
 * Created by yiannis.pipertzis on 30/04/2015.
 */
DrinksCollection = new Mongo.Collection("drinks");


DrinksSchema = new SimpleSchema({
  name: {
    type: String
  },
  price: {
    type: Number,
    decimal: true,
    optional: true
  },
  imgURL: {
    type: String,
    optional: true
  },
  numStock: {
    type: Number,
    optional: true
  }
});


DrinksCollection.attachSchema(DrinksSchema);
