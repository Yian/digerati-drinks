/**
 * Created by yiannis.pipertzis on 30/04/2015.
 */
CurrencyCollection = new Mongo.Collection("currency");

CurrencySchema = new SimpleSchema({
  value: {
    type: Number
  },
  imgURL: {
    type: String
  }
});

CurrencyCollection.attachSchema(CurrencySchema);