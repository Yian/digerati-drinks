/**
 * Created by yiannis.pipertzis on 30/04/2015.
 */
UsersCollection = new Meteor.Collection("users");

UsersSchema = new SimpleSchema({
  name: {
    type: String
  },
  imgURL: {
    type: String,
    optional: true
  }
});

UsersCollection.attachSchema(UsersSchema);
