/**
 * Created by yiannis.pipertzis on 30/04/2015.
 */
UserPurchasesCollection = new Mongo.Collection("userpurchases");


DrinksSchema = new SimpleSchema({
  name: {
    type: String
  },
  price: {
    type: Number,
    decimal: true,
    optional: true
  },
  imgURL: {
    type: String,
    optional: true
  },
  quantity: {
    type: Number
  }
});


UserPurchasesSchema = new SimpleSchema({
  userId: {
    type: String
  },
  dateOfPurchase: {
    type: Date
  },
  drinks: {
    type: [DrinksSchema]
  }
});

UserPurchasesCollection.attachSchema(UserPurchasesSchema);