/**
 * Created by yiannis.pipertzis on 30/04/2015.
 */
Meteor.publish("users", function() {
  return UsersCollection.find({});
});

Meteor.publish("drinks", function() {
  return DrinksCollection.find({});
});

Meteor.publish("currency", function() {
  return CurrencyCollection.find({});
});

Meteor.publish("userPurchases", function() {
  return UserPurchasesCollection.find({});
});

Meteor.publish("userPayments", function() {
  return UserPaymentsCollection.find({});
});