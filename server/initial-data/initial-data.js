/**
 * Created by yiannis.pipertzis on 30/04/2015.
 */
//Add initial data
Meteor.startup(function () {
  //UsersCollection.remove({});

  if (!UsersCollection.find({}).count() > 0) {
    UsersCollection.insert({name: "Yiannis Pipertzis",imgURL: '/images/yian.png'});
    UsersCollection.insert({name: "Andrew Harward",imgURL: '/images/andrew.png'});
    UsersCollection.insert({name: "Neill horsman",imgURL: '/images/neill.png'});
    UsersCollection.insert({name: "Ryan Stafford",imgURL: '/images/ryan.png'});
    UsersCollection.insert({name: "Tim Faase",imgURL: '/images/tim.png'});
    UsersCollection.insert({name: "Georgia Gregory",imgURL: '/images/georgia.png'});
    UsersCollection.insert({name: "Kristen Fornarsi",imgURL: '/images/kristen.png'});
    UsersCollection.insert({name: "Steve Martin",imgURL: '/images/steven.png'});
    UsersCollection.insert({name: "Tegan Vallins",imgURL: '/images/tegan.png'});
    UsersCollection.insert({name: "Adam Cook",imgURL: '/images/adam.png'});
    UsersCollection.insert({name: "Ainsley Freeman",imgURL: '/images/ainsley.png'});
  }

 // DrinksCollection.remove({});

  if (!DrinksCollection.find({}).count() > 0) {
    DrinksCollection.insert({name: "Coke", price: 1, imgURL: '/images/coke.png'});
    DrinksCollection.insert({name: "Diet Coke", price: 1, imgURL: '/images/diet-coke.png'});
    DrinksCollection.insert({name: "Coffee", price: 1, imgURL: '/images/coffee.png'});
  }

 CurrencyCollection.remove({});

  if (!CurrencyCollection.find({}).count() > 0) {
    CurrencyCollection.insert({value: 50, imgURL: 'images/currency_50.jpg'});
    CurrencyCollection.insert({value: 10, imgURL: 'images/currency_10.jpg'});
    CurrencyCollection.insert({value: 2, imgURL: 'images/currency_2.png'});
    CurrencyCollection.insert({value: 20, imgURL: 'images/currency_20.jpg'});
    CurrencyCollection.insert({value: 5, imgURL: 'images/currency_5.jpg'});
    CurrencyCollection.insert({value: 1, imgURL: 'images/currency_1.png'});
  }
});