/**
 * Created by yiannis.pipertzis on 30/04/2015.
 */
Meteor.methods({
  insertPurchases: function(userId, Drinks) {
    UserPurchasesCollection.insert({userId: userId, dateOfPurchase: new Date(), drinks: Drinks});
  },
  insertPayments: function(userId, amountPaid) {
    UserPaymentsCollection.insert({userId: userId, dateOfPayment: new Date(), amountPaid: amountPaid});
  },
  incrementDrinkStock: function(drink, amount) {
    DrinksCollection.update({name: drink.name}, {$inc: {numStock: amount}});
  }
});