/**
 * Created by yiannis.pipertzis on 30/04/2015.
 */
CartItemsCollection = new Mongo.Collection(null);

DrinksSchema = new SimpleSchema({
  name: {
    type: String
  },
  price: {
    type: Number,
    decimal: true,
    optional: true
  },
  imgURL: {
    type: String,
    optional: true
  }
});

CartItemsSchema = new SimpleSchema({
  quantity: {
    type: Number
  },
  drink: {
    type: DrinksSchema
  }
});

CartItemsCollection.attachSchema(CartItemsSchema);
