/**
 * Created by yiannis.pipertzis on 30/04/2015.
 */
Router.configure({
  // this will be the default controller

});

MainRoutingController = RouteController.extend({
  waitOn: function () {
    return [Meteor.subscribe('users'),
      Meteor.subscribe('drinks'),
      Meteor.subscribe('currency'),
      Meteor.subscribe('userPurchases'),
      Meteor.subscribe('userPayments')]
  },
  data: function () {
    return {
      users: UsersCollection.find({}),
      drinks: DrinksCollection.find({}),
      currency: CurrencyCollection.find({}),
      userPurchases: UserPurchasesCollection.find({})
    };
  }
});

Router.route('/', function () {
  this.layout('mainLayout');
  this.render('main');
  this.render('header', {to:'header'});
}, {
  name: 'home',
  controller: MainRoutingController
});

Router.route('/admin', function () {
  this.layout('mainLayout');
  this.render('admin');
  this.render('adminHeader', {to:'header'});
}, {
  name: 'admin',
  controller: MainRoutingController
});