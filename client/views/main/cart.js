/**
 * Created by yiannis.pipertzis on 30/04/2015.
 */
Template.cart.helpers({
    cartItems: function() {
      return CartItemsCollection.find({});
    }
});
