/**
 * Created by yiannis.pipertzis on 30/04/2015.
 */
Template.main.created = function () {
  Session.set('total', 0);
  Session.set('message', '');

};

Template.main.events({
  'click .payment li': function () {
    Session.set('total', Session.get('total') + this.value);
  },
  'click #clearTotal': function () {
    Session.set('total', 0);
  },
  'click .products li': function (evt,tmp) {

   // console.log($(evt.currentTarget).css('display','block'));
    $('.confirm').removeClass('show');
    $(evt.currentTarget).find('.confirm').addClass('show');


    //check if drink already exists
 /*   console.log(this.name);
    if (CartItemsCollection.find({"drink.name": this.name}).count() > 0) {
      CartItemsCollection.update({"drink.name": this.name}, {$inc: {quantity: 1}});
    } else {
      CartItemsCollection.insert({drink: this, quantity: 1});
    }*/
  },
  'click #makePayment': function () {
    var userId = UsersCollection.findOne({name: Session.get('currentUser')})._id;

    if (Session.get('total') >> 0) {
      Meteor.call('insertPayments', userId, Session.get('total'), function (err, data) {
        if (err) {
          console.log(err);
        } else {
          Session.set('message', 'thank you');
          Session.set('total', 0);
        }
      });
    } else {
      Session.set('message', 'Please select a dollar amount');
    }

    /*    var userId = UsersCollection.findOne({name: Session.get('currentUser')})._id;
     var Drinks = [];

     CartItemsCollection.find({}).forEach(function (cartItem) {
     cartItem.drink.quantity = cartItem.quantity;
     Drinks.push(cartItem.drink);
     });

     Meteor.call('insertPurchases', userId, Drinks, function (err, data) {
     if (err) {
     console.log(err);
     } else {
     CartItemsCollection.remove({});
     }
     });*/
  },
  'click .confirm.show': function () {
    var userId = UsersCollection.findOne({name: Session.get('currentUser')})._id;
    var Drinks = [];

    var drink = this;
    drink.quantity = 1;

    Drinks.push(drink);

    Meteor.call('insertPurchases', userId, Drinks, function (err, data) {
      if (err) {
        console.log(err);
      } else {
        Meteor.call('incrementDrinkStock', drink, -1);
        $('.confirm').removeClass('show');
      }
    });
  }
});

Template.main.helpers({
  total: function () {
    return Session.get('total');
  },
  getMessage: function () {
    return Session.get('message');
  },
  paymentDisabledAttr: function() {
    return Session.get('total') <= 0 ? "disabled" : "";
  },
  userPurchases: function () {
    var user = UsersCollection.findOne({name: Session.get('currentUser')});

    console.log(user);
    if (user) {
     /*
      var groupedDates = _.groupBy(_.pluck(UserPurchasesCollection.find({userId: user._id}).fetch(), 'dateOfPurchase'));

      _.each(_.values(groupedDates), function (dates) {
        console.log({Date: dates[0], Total: dates.length});
      });*/
      return UserPurchasesCollection.find({userId: user._id});
    }
  },
  userActions: function() {
    var user = UsersCollection.findOne({name: Session.get('currentUser')});

    if (user) {
      var purchases = UserPurchasesCollection.find({userId: user._id}, {limit: 20}).fetch();
      var payments = UserPaymentsCollection.find({userId: user._id}, {limit: 20}).fetch();

      var result = _.map(purchases, function (p) {
        return {type: 'purchase', date: p.dateOfPurchase, description: getPurchaseDescription(p)}
      });
      result = result.concat(_.map(payments, function (p) {
        return {type: 'payment', date: p.dateOfPayment, description: 'Paid: $' + p.amountPaid }
      }));

      result = _.sortBy(result, 'date');
      result.reverse();

      return result;
    }
  },
  getPurchaseDate: function (date) {
    return date.format("dd/mm/yyyy");
  },
  getPurchaseTime: function (date) {
    return date.format("hh:MM");
  },
  getAmountOwing: function () {
    var total = 0;
    var user = UsersCollection.findOne({name: Session.get('currentUser')});

    if (user) {
      UserPurchasesCollection.find({userId: user._id}).forEach(function (purchase) {
        purchase.drinks.forEach(function (drink) {
          total += drink.price;
        })
      });
      UserPaymentsCollection.find({userId: user._id}).forEach(function (payment) {
        total -= payment.amountPaid;
      });
      console.log(total);
      return total;
    }
  }
});

function getPurchaseDescription(purchase) {
  return 'Drank ' + _.map(purchase.drinks, function(d) { return d.quantity + ' ' + d.name + (d.quantity > 1 ? 's' : ''); }).join(", ");
}
