/**
 * Created by yiannis.pipertzis on 30/04/2015.
 */
Template.header.created = function () {
  Session.set('currentUser', "No user selected");
};

Template.header.helpers({
  currentUser: function () {
    return Session.get('currentUser');
  },
  currentProfilePic: function () {
    var imageToDisplay = '/images/avatar.png';

    var user = UsersCollection.findOne({name: Session.get('currentUser')});
    if (user) {
      if (user.imgURL) {
        imageToDisplay = user.imgURL;
      }
    }
    return imageToDisplay;
  }
});

Template.header.events({
  'click .user-list li': function () {
    Session.set('currentUser', this.name);
  },
  'click #selectUser': function () {
    $('#users').slideDown();
  },
  'click #users a': function () {
    $('#users').slideToggle();
  }
});